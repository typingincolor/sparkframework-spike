package com.losd.spark;


import spark.ModelAndView;
import spark.TemplateViewRoute;
import spark.template.mustache.MustacheTemplateEngine;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.get;

/**
 * Created by abraithwaite on 07/01/15.
 */
public class Application {
    public static void main(String[] args) {


        get("/", (req, res) -> "Go away!");

        TemplateViewRoute hello = (req, res) -> {
            Map map = new HashMap();
            map.put("name", "Andrew");
            return new ModelAndView(map, "hello.mustache");
        };

        get("/hello", hello, new MustacheTemplateEngine());
    }
}
